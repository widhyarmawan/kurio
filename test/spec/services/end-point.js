'use strict';

describe('Service: endPoint', function () {

  // load the service's module
  beforeEach(module('kurioApp'));

  // instantiate service
  var endPoint;
  beforeEach(inject(function (_endPoint_) {
    endPoint = _endPoint_;
  }));

  it('should do something', function () {
    expect(!!endPoint).toBe(true);
  });

});
