'use strict';

describe('Service: scripts/services/endPoint', function () {

  // load the service's module
  beforeEach(module('kurioApp'));

  // instantiate service
  var scripts/services/endPoint;
  beforeEach(inject(function (_scripts/services/endPoint_) {
    scripts/services/endPoint = _scripts/services/endPoint_;
  }));

  it('should do something', function () {
    expect(!!scripts/services/endPoint).toBe(true);
  });

});
