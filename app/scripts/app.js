'use strict';

/**
 * @ngdoc overview
 * @name kurioApp
 * @description
 * # kurioApp
 *
 * Main module of the application.
 */
angular
.module('kurioApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'angularTrix',
    'toastr'
])
.config(function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
    })
    .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
    })
    .otherwise({
        redirectTo: '/'
    });

    $locationProvider.hashPrefix('');
})
.run(function($rootScope, $location, config){


    $rootScope.imageUrl = function(path){
        return config.imageHost + path;
    };

});
