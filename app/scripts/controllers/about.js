'use strict';

/**
 * @ngdoc function
 * @name kurioApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the kurioApp
 */
angular.module('kurioApp')
.controller('AboutCtrl', function ($rootScope) {
    $rootScope.menu = 'about';
});
