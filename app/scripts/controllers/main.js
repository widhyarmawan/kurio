'use strict';

/**
 * @ngdoc function
 * @name kurioApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kurioApp
 */
angular.module('kurioApp')
.controller('MainCtrl', function ($scope, $rootScope, $http, $location, $routeParams, shared, config, $uibModal, toastr) {
    $rootScope.menu = 'home';
    $scope.imagePath = config.imagePath;
    $scope.articles  = {};

    function getArticles(){
        $scope.getArticlesLoader = true;
        $http({
            method: 'GET',
            url: shared.article_api
        })
        .then(function success(response){
            $scope.getArticlesLoader = false;
            if (response.data.status_code && response.data.status_code === 200){
                var contents    = response.data.contents;
                $scope.articles = contents.articles;
            }
        }, function error(response){
            console.log(response);
        });
    }
    getArticles();

    function addToArray(article){
        $scope.articles.unshift(article);
    }

    function updateToArray(newArticle){
        var index = $scope.articles.findIndex(function(article){
            return article.uuid === newArticle.uuid;
       });
       $scope.articles[index] = newArticle;
    }

    function deleteArray(articleUuid){
        var index = $scope.articles.findIndex(function(article){
            return article.uuid === articleUuid;
       });
       $scope.articles.splice(index, 1);
    }

    $scope.deleteArticle = function(articleUuid){
        swal({
            title : 'Are you sure?',
            type  : 'question',
            showCancelButton   : true,
            confirmButtonColor : '#3085d6',
            cancelButtonColor  : '#ac2925',
            confirmButtonText  : 'Yes, delete it!'
        })
        .then(function (response) {
            if (response.value === true){
                $http({
                    method: 'DELETE',
                    url   : shared.article_api + '/' + articleUuid
                })
                .then(function success(response){
                    if (response.data.status_code && response.data.status_code === 200){
                        deleteArray(articleUuid);
                        toastr.success('Delete article', 'Success!');
                    }
                }, function error(response){
                    console.log(response);
                });
            }
        }).catch(function(err){
            console.log(err);
        });

    };

    $scope.articleSelected = {};
    $scope.deleteMultipleArticle = function(articleSelected){
        console.log(articleSelected);
        var articleUuids = [];
        angular.forEach(articleSelected, function(value, articleUuid){
            if (value === true){
                articleUuids.push(articleUuid);
            }
        });

        if(articleUuids.length === 0){
            swal('Oops..,Sorry!', 'You have not selected any article');
            return false;
        }

        var deleteMultipleArticleData = {
            article_uuids : articleUuids
        };

        swal({
            title : 'Are you sure?',
            text  : "You won't to delete this data!",
            type  : 'question',
            showCancelButton   : true,
            confirmButtonColor : '#3085d6',
            cancelButtonColor  : '#ac2925',
            confirmButtonText  : 'Yes, delete it!'
        })
        .then(function () {
            $scope.loaderDelete = 1;
            $http({
                method : 'POST',
                url    : shared.article_api + '/multiple-delete',
                data   : deleteMultipleArticleData
            })
            .then(function success(response){
                if (response.data.status_code && response.data.status_code === 200){
                    var contents = response.data.contents;
                    $scope.articles = contents.articles;
                    $scope.isDeleteAll = false;
                }
            },function error(response){
                console.log(response);
            });
          
        }).catch(function(err){
            console.log(err);
        });
    };

    $scope.addOrEditArticle = function (type, articleUuid) {
        console.log(articleUuid);
        $uibModal.open({
            backdrop     : 'static',
            keyboard     : false,
            templateUrl  : 'views/modals/add-edit-article.html',
            controller   : 'ModalAddEditArticleCtrl',
            size         : 'lg',
            resolve: {
                data: function(){
                    var data = {
                        'type'        : type,
                        'articleUuid' : articleUuid
                    };
                    return data;
                }
            }
        }).result.then(function(article) {
            if (type === 'new'){
                addToArray(article);
            } else {
                updateToArray(article);
            }
        });
    };

    $scope.isDeleteAll = false;
    $scope.chcekSelected = function(articleSelected){
        var articleUuids = [];
        angular.forEach(articleSelected, function(value, articleUuid){
            if (value === true){
                articleUuids.push(articleUuid);
            }
        });
        if (articleUuids.length > 0){
            $scope.isDeleteAll = true;
        } else {
            $scope.isDeleteAll = false;
        }
    };

    $scope.selectedAll = false;
    $scope.checkAll = function (selectedAll) {
        $scope.selectedAll = selectedAll;
        if ($scope.selectedAll === true) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        $scope.articleSelected = {};
        angular.forEach($scope.articles, function (article) {
            $scope.articleSelected[article.uuid] =  $scope.selectedAll;
        });
        $scope.chcekSelected($scope.articleSelected);
    };

})
.controller('ModalAddEditArticleCtrl', function($scope, $rootScope, $http, shared, $uibModalInstance, data, toastr){
    $scope.type        = data.type;
    $scope.articleUuid = data.articleUuid;

    $scope.options = {
        title     : 'Add Article',
        submitBtn : 'Save'
    };
    $scope.article = {
        title    : '',
        content : '',
        image    : '',
    };

    function getArticle(articleUuid){
        $http({
            method: 'GET',
            url   : shared.article_api + '/' + articleUuid
        })
        .then(function success(response){
            if (response.data.status_code && response.data.status_code === 200){
                var contents   = response.data.contents;
                $scope.article = contents.article;
            }
        }, function error(response){
            console.log(response);
        });
    }

    if ($scope.type === 'edit'){
        $scope.options = {
            title     : 'Edit Article',
            submitBtn : 'Update'
        };
        getArticle($scope.articleUuid);
    }
    
    $scope.submit = function(article){
        if (article.title === ''){
            swal('Oops..,Sorry!', 'Title is required');
            return false;
        }
        if (article.content === ''){
            swal('Oops..,Sorry!', 'Content is required');
            return false;
        }
        if ($scope.type === 'new' && $scope.files === undefined){
            swal('Oops..,Sorry!', 'Image is required');
            return false;
        }

        var fd = new FormData();
        fd.append('article_uuid', $scope.articleUuid);
        fd.append('title', article.title);
        fd.append('content', article.content);
        fd.append('type', $scope.type);
        if ($scope.files !== undefined){
            fd.append('file', $scope.files[0]);
        }
        $scope.saveLoader = true;
        $http.post(shared.article_api, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         })
        .then(function success(response){
            $scope.saveLoader = false;
            if (response.data.status_code && response.data.status_code === 200){
                var contents    = response.data.contents;
                toastr.success($scope.options.title, 'Success!');
                $uibModalInstance.close(contents.article);
            }
        }, function error(response){
            $scope.saveLoader = false;
            console.log(response);
        });
    };

    $scope.cancel = function(){
        $uibModalInstance.dismiss();
    };

    $scope.uploadedFile = function(element) {
        var reader = new FileReader();
        reader.onload = function(event) {
         $scope.$apply(function($scope) {
            $scope.files = element.files;
                $scope.src = event.target.result;
            });
        };
        reader.readAsDataURL(element.files[0]);
    };
});
