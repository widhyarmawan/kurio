'use strict';

/**
 * @ngdoc service
 * @name kurioApp.config
 * @description
 * # config
 * Factory in the kurioApp.
 */
angular.module('kurioApp')
.factory('config', function ($location) {
    var config = {
        
        production : {
            endPoint  : '',
            imageHost : ''
        },
        staging : {
            endPoint  : '',
            imageHost : ''
        },
        local : {
            endPoint  : 'http://localhost:8000/api',
            imageHost : 'http://localhost:8000/'
        }
    };

    var host = $location.host();

    //Edit Server Here
    var server = config.production;
    if (host === 'localhost'){
        server = config.local;        
    }

    return {
        apiEndPoint : server.endPoint,
        imageHost   : server.imageHost
    };
});
