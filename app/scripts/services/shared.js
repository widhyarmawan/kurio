'use strict';

/**
 * @ngdoc service
 * @name kurioApp.shared
 * @description
 * # shared
 * Factory in the kurioApp.
 */
angular.module('kurioApp')
.factory('shared', function (config) {

    var apiEndPoint = config.apiEndPoint;
    
    // Public API here
    return {
        article_api  : apiEndPoint + '/articles',
    };
});
